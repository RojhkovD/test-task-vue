import Vue from 'vue'
import Router from 'vue-router'
import baseComponent from '@/components/baseComponent'
import draggableSortable from '@/components/draggableSortable'
import diagram from '@/components/diagram'
import animation from '@/components/animation'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
    },
    {
      path: '/draggable-sortable',
      name: 'draggableSortable',
      component: draggableSortable
    },
    {
      path: '/diagram',
      name: 'diagram',
      component: diagram
    },
    {
      path: '/animation',
      name: 'animation',
      component: animation
    }
  ]
})
